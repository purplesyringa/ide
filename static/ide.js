class IDE {
	constructor() {
		this.codebox = new Codebox();
		this.loadWorkspaces();

		this.wasChangedSinceLastSave = false;
		this.codebox.stateChangeHandler = state => {
			this.wasChangedSinceLastSave = true;
			this.currentWorkspace.stateChangeHandler(state);
		};
	}


	async loadWorkspaces() {
		this.workspaces = (await API.getWorkspaces()).map(workspace => {
			return new Workspace(workspace, this);
		});
		this.currentWorkspace = this.workspaces[0];

		this.updateWorkspace();

		setTimeout(() => {
			this.save();
		}, 5000);
	}


	serialize() {
		return {
			workspaces: this.workspaces.map(w => w.serialize()),
			currentWorkspace: this.workspaces.indexOf(currentWorkspace)
		};
	}


	async save() {
		if(this.wasChangedSinceLastSave) {
			this.wasChangedSinceLastSave = false;
			await API.saveIDEState(this.serialize());
			setTimeout(() => this.save(), 5000);
		}
	}


	updateWorkspace() {
		const workspacesNode = document.querySelector("#workspaces");
		workspacesNode.innerHTML = "";

		for(const workspace of this.workspaces) {
			const workspaceNode = document.createElement("div");
			workspaceNode.className = "workspace";
			if(workspace === this.currentWorkspace) {
				workspaceNode.classList.add("current");
			}
			workspaceNode.appendChild(document.createTextNode(workspace.name));
			workspacesNode.appendChild(workspaceNode);
		}

		this.currentWorkspace.update();
	}


	async shortcutOpenFile() {
		const path = (await API.openFile()).path;
		if(!path) {
			return;
		}

		const file = await API.getFile(path);
		if(!file.ok) {
			return;
		}

		const text = atob(file.data);
		this.currentWorkspace.addFile(path, text);
	}
}

const ide = new IDE();


document.onkeydown = e => {
	if(e.ctrlKey || e.altKey || e.key === "Backspace" || e.key === "F5/**/" || e.key === "F6") {
		e.preventDefault();
	}

	if(e.ctrlKey && e.key === "o") {
		ide.shortcutOpenFile();
	}
};