class Tab {
	constructor({name, path, state, closeable}) {
		this.name = name;
		this.path = path;
		this.state = state;
		this.closeable = closeable;
	}


	serialize() {
		return {
			name: this.name,
			path: this.path,
			state: this.state,
			closeable: this.closeable
		};
	}
}