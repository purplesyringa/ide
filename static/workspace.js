class Workspace {
	constructor({name}, ide) {
		this.name = name;
		this.ide = ide;
		this.tabs = [
			new Tab({
				name: "Notes",
				path: "s22v://notes",
				state: {
					content: "",
					ranges: [],
					scrollLeft: 0,
					scrollTop: 0
				},
				closeable: false
			})
		];
		this.currentTab = this.tabs[0];
	}


	update() {
		const tabsNode = document.querySelector("#tabs");
		tabsNode.innerHTML = "";

		for(const tab of this.tabs) {
			const tabNode = document.createElement("div");
			tabNode.className = "tab";
			if(tab === this.currentTab) {
				tabNode.classList.add("current");
			}

			tabNode.onclick = () => {
				this.currentTab = tab;
				this.update();
			};

			tabNode.appendChild(document.createTextNode(tab.name));

			if(tab.closeable) {
				const closeNode = document.createElement("div");
				closeNode.className = "close";
				closeNode.innerHTML = "&times";
				tabNode.appendChild(closeNode);
			}

			tabsNode.appendChild(tabNode);
		}

		this.ide.codebox.setState(this.currentTab.state);
	}


	stateChangeHandler(state) {
		this.currentTab.state = state;
	}


	serialize() {
		return {
			tabs: this.tabs.map(tab => tab.serialize()),
			currentTab: this.tabs.indexOf(this.currentTab)
		};
	}


	addFile(path, content) {
		let tab = this.tabs.find(t => t.path === path);
		if(!tab) {
			tab = new Tab({
				name: path.split("/").slice(-1)[0],
				path,
				state: {
					content,
					ranges: [],
					scrollLeft: 0,
					scrollTop: 0
				},
				closeable: true
			});
			this.tabs.splice(this.tabs.indexOf(this.currentTab) + 1, 0, tab);
		}
		this.currentTab = tab;

		this.saveWorkspace();

		this.update();
	}
}