class Codebox {
	constructor() {
		this.editor = ace.edit("codebox");
		this.editor.setTheme("ace/theme/dracula");
		this.editor.session.setMode("ace/mode/python");
		this.editor.setOption("showPrintMargin", false);

		this.editor.on("change", () => {
			this.onStateChange();
		});
		this.editor.on("changeScrollLeft", () => {
			this.onStateChange();
		});
		this.editor.on("changeScrollTop", () => {
			this.onStateChange();
		});
		this.editor.session.getSelection().on("changeCursor", () => {
			this.onStateChange();
		});
		this.editor.session.getSelection().on("changeSelection", () => {
			this.onStateChange();
		});

		this.isSettingState = false;
	}


	onStateChange() {
		if(this.isSettingState) {
			// Don't react to change in progress
			return;
		}
		this.stateChangeHandler({
			content: this.editor.getValue(),
			ranges: this.editor.session.getSelection().getAllRanges().map(r => {
				return {
					start: {...r.start},
					end: {...r.end}
				};
			}),
			scrollLeft: this.editor.session.getScrollLeft(),
			scrollTop: this.editor.session.getScrollTop()
		});
	}


	setState({content, ranges, scrollLeft, scrollTop}) {
		this.isSettingState = true;
		this.editor.focus();
		this.editor.setValue(content);
		const selection = this.editor.session.getSelection();
		selection.clearSelection();
		for(const r of ranges) {
			selection.addRange(new ace.Range(r.start.row, r.start.column, r.end.row, r.end.column), false);
		}
		this.editor.session.setScrollLeft(scrollLeft);
		this.editor.session.setScrollTop(scrollTop);
		setTimeout(() => {
			this.isSettingState = false;
		}, 0);
	}
}