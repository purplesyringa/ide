from tkinter import Tk
from tkinter.filedialog import askopenfilename
import base64
import json
import os


def readData(name, default):
	try:
		with open(os.path.join(os.path.dirname(__file__), "data", name + ".json")) as f:
			return json.loads(f.read())
	except OSError:
		return default


class RPC:
	def getWorkspaces(self):
		return readData("workspaces", [
			{
				"name": "Default"
			}
		])


	def openFile(self):
		# I haven't found a better way yet
		tk = Tk()
		tk.withdraw()
		tk.overrideredirect(True)
		tk.geometry("0x0+0+0")
		tk.deiconify()
		tk.lift()
		path = askopenfilename(parent=tk)
		tk.destroy()
		return {
			"path": path
		}


	def getFile(self, path):
		try:
			with open(path, "rb") as f:
				return {
					"data": base64.b64encode(f.read()).decode(),
					"ok": True
				}
		except OSError:
			return {
				"ok": False
			}


	def saveIDEState(self, state):
		print(json.dumps(state, indent=1))
		return {"ok": True}