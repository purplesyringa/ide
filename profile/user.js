user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);
user_pref("app.normandy.first_run", false);
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
user_pref("trailhead.firstrun.didSeeAboutWelcome", true);
user_pref("browser.tabs.drawInTitlebar", false);
user_pref("security.data_uri.block_toplevel_data_uri_navigations", false);